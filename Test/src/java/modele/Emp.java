/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import annotation.Classe;
import annotation.Url;
import java.util.ArrayList;
import java.util.HashMap;
import utilitaire.ModeleView;
import annotation.ForeignKey;
import annotation.PrimaryKey;

/**
 *
 * @author ASUS
 */
@Classe( classe = true, name = "Emp" )
public class Emp {
    
    @PrimaryKey( isPrimaryKey = true )
    int id;
    String nomEmp; 
    @ForeignKey( isForeignKey = true )
    Dept d;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomEmp() {
        return nomEmp;
    }

    public void setNomEmp(String nomEmp) {
        this.nomEmp = nomEmp;
    }

    public Dept getD() {
        return d;
    }

    public void setD(Dept d) {
        this.d = d;
    }

    public Emp(int id, String nomEmp, Dept d) {
        this.id = id;
        this.nomEmp = nomEmp;
        this.d = d;
    }
    
    @Url( name = "create" )
    public ModeleView create(){
        HashMap <String,Object> hmap = new HashMap<String,Object>();
        hmap.put("Employe",this);
        ModeleView modele = new ModeleView("/DetailEmp.jsp",hmap);
        return modele;
    }
    
    @Url( name = "formulaire" )
    public ModeleView formulaire(){
        HashMap <String,Object> hmap = null;
        ModeleView modele = new ModeleView("/formulaire.jsp",hmap);
        return modele;
    }
    
    public Emp(){
        
    }
}
