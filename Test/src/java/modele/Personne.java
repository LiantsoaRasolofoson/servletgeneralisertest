/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import annotation.Classe;
import annotation.PrimaryKey;
import annotation.Url;
import java.util.HashMap;
import utilitaire.ModeleView;

/**
 *
 * @author ASUS
 */
@Classe( classe = true, name = "Personne" )
public class Personne {
    
    @PrimaryKey( isPrimaryKey = true )
    int id;
    String nom;
    int idDept;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getIdDept() {
        return idDept;
    }

    public void setIdDept(int idDept) {
        this.idDept = idDept;
    }

    public Personne(int id, String nom, int idDept) {
        this.id = id;
        this.nom = nom;
        this.idDept = idDept;
    }
    
    public Personne(){
        
    }
    
    @Url( name = "create" )
    public ModeleView create(){
        HashMap <String,Object> hmap = new HashMap<String,Object>();
        hmap.put("Personne",this);
        ModeleView modele = new ModeleView("/DetailPersonne.jsp",hmap);
        return modele;
    }
    
    @Url( name = "formulaire" )
    public ModeleView formulaire(){
        HashMap <String,Object> hmap = null;
        ModeleView modele = new ModeleView("/formulairePersonne.jsp",hmap);
        return modele;
    }
    
}
