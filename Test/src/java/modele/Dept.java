/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import annotation.Classe;
import annotation.Url;
import java.util.HashMap;
import utilitaire.ModeleView;
import annotation.ForeignKey;
import annotation.PrimaryKey;
import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
@Classe( classe = true, name = "Dept" )
public class Dept {
    
    @PrimaryKey( isPrimaryKey = true )
    int id;
    String nom;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Dept(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }
    
    @Url( name = "hello" )
    public ModeleView hello(){
        HashMap <String,Object> hmap = new HashMap<String,Object>();
        hmap.put("Hello","Hello World!");
        ModeleView modele = new ModeleView("/hello.jsp",hmap);
        return modele;
    }
    
    @Url( name = "findAll" )
    public ModeleView findAll(){
        ArrayList <Dept> array = new ArrayList<Dept>();
        array.add(new Dept(1,"RH"));
        array.add(new Dept(2,"Comptabilite"));
        HashMap <String,Object> hmap = new HashMap<String,Object>();
        hmap.put("Hello",array);
        ModeleView modele = new ModeleView("/listeDept.jsp",hmap);
        return modele;
    }
    public Dept(){
        
    }
}
