<%-- 
    Document   : DetailEmp
    Created on : 8 nov. 2022, 22:26:32
    Author     : ASUS
--%>
<%@page import="modele.Personne"%>
<%@page import="java.util.HashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    HashMap<String, Object> data = (HashMap<String, Object>)request.getAttribute("data");
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% 
            for(String key : data.keySet() ){
                Personne p = (Personne) data.get(key);
        %>
            <h1>Bonjour <% out.print(p.getNom()); %>, votre id est <% out.print(p.getId()); %></h1>
            <p>Département Id: <% out.print(p.getIdDept()); %></p>
        <% } %>
    </body>
</html>
