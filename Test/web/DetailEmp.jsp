<%-- 
    Document   : DetailEmp
    Created on : 8 nov. 2022, 22:26:32
    Author     : ASUS
--%>
<%@page import="modele.Emp"%>
<%@page import="java.util.HashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    HashMap<String, Object> data = (HashMap<String, Object>)request.getAttribute("data");
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% 
            for(String key : data.keySet() ){
                Emp employe = (Emp) data.get(key);
        %>
            <h1>Bonjour <% out.print(employe.getNomEmp()); %>, votre id est <% out.print(employe.getId()); %></h1>
            <p>Département Id: <% out.print(employe.getD().getId()); %></p>
        <% } %>
    </body>
</html>
