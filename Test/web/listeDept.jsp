<%-- 
    Document   : listeDept
    Created on : 8 nov. 2022, 21:21:58
    Author     : ASUS
--%>

<%@page import="modele.Dept"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    HashMap<String, Object> data = (HashMap<String, Object>)request.getAttribute("data");
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Liste Dept</h1>
        <table border="1" cellspacing="0" width="250px">
            <tr>
                <th>Id</th>
                <th>Nom</th>
            </tr>
        <%  for(String key : data.keySet() ){ 
                ArrayList <Dept> array = (ArrayList <Dept>)data.get(key); 
                for(int i=0; i<array.size(); i++){ 
        %>
            <tr>
                <td><% out.print(array.get(i).getId()); %></td>
                <td><% out.print(array.get(i).getNom()); %></td>
            </tr>
        <%       
                } 
            } 
        %>
        </table>
    </body>
</html>
