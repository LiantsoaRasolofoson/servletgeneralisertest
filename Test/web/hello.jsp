<%-- 
    Document   : hello
    Created on : 8 nov. 2022, 21:11:15
    Author     : ASUS
--%>

<%@page import="java.util.HashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    HashMap<String, Object> data = (HashMap<String, Object>)request.getAttribute("data");
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% for(String key : data.keySet() ){ %>
            <h1><% out.print(data.get(key)); %></h1>
        <% } %>
    </body>
</html>
